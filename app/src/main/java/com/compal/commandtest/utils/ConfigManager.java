package com.compal.commandtest.utils;

import android.util.Log;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.Arrays;
import java.util.ArrayList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.w3c.dom.Element;

import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

public class ConfigManager {
    private static final String TAG = ConfigManager.class.getSimpleName();
    private static int search_index;
    //The following are the definition of the config file search path
    public static final String CONFIG_DEFAULT_PATH = "";
    public static final String CONFIG_FROM_ETC = "/etc/";
    public static final String CONFIG_FROM_SDCARD = "/storage/sdcard0/";
    public static final String CONFIG_FROM_DATA = ConfigFileParse.getConfigFilePath();
    public static final String CONFIG_OVERRIDE_PATH = ConfigFileParse.getConfigFilePath() + "override.xml";

    //The following are the definition of the config files
    public static final String CONFIG_FILE_ALL = "all.xml";
    public static final String CONFIG_FILE_OVERLAY = "overlay.xml";

    public static final String CONFIG_XML_NAMESPACE = "application";
    public static final String CONFIG_XML_CATEGORY_SMR = "smr";
    public static final String CONFIG_XML_TAG_GENERIC = "generic";

    private static final String DEFINE_NAME = "name";
    private static final String DEFINE_ACTION = "action";
    private static final String DEFINE_DELETE = "delete";

    public static final String CONFIG_XML_CATEGORY_ITEM = "item-config";

    public Document xmlDocument = null;
    private static String[] default_paths = {CONFIG_DEFAULT_PATH, CONFIG_FROM_ETC, CONFIG_FROM_SDCARD, CONFIG_FROM_DATA};
    private static ArrayList<String> overlayCategory = new ArrayList<String>();
    private static final String XML_ROOT_EXPRESSION = "/application/device[@platform='%s' and @model='%s' and @sku='%s']";
    private static final String XML_CATEGORY_EXPRESSION = "category[@name='%s']";
    private static final String XML_ITEM_EXPRESSION = "item[@name='%s']";

    public ConfigManager(String path, String filename) throws Exception {
        int start = 1;
        String fullPath;

        //If user uses the path other than the default paths
        if (path != "") {
            default_paths[0] = path;
            start = 0;
        }

        for (search_index = start; search_index < default_paths.length; search_index++) {
            fullPath = default_paths[search_index] + filename;
            if (domInit(fullPath)) {
                break;
            }
        }

        if (xmlDocument == null) {
            throw new FileNotFoundException("File not found:" + filename+ ", default_paths:" + Arrays.toString(default_paths));
        }
    }

    public static String getConfigFilePath() {
        return default_paths[search_index];
    }

    private boolean domInit(String path) {
        boolean result = false;
        try {
            File file = null;
            FileInputStream fileInput;

            file = new File(path);
            fileInput = new FileInputStream(file);
            DocumentBuilderFactory builderFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder builder = builderFactory.newDocumentBuilder();
            xmlDocument = builder.parse(fileInput);
            result = true;
        } catch (Exception e) {
            Log.e(TAG, e.getMessage());
        }
        return result;
    }

    public static Document getDom(String category) {
        return ConfigFileParse.getDom(category);
    }

    public boolean setDom(Document doc) {
        if (doc == null) {
            return false;
        }
        xmlDocument = doc;
        return true;
    }

    public static Document createDocWithNodes(ArrayList<Node> nodeListArray,  Element deviceElement) {
        Document doc = null;
        try {
            DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
            doc = docBuilder.newDocument();
            // Make the new node an actual item in the target document
            doc.adoptNode((Node)deviceElement);
            Element rootElement = doc.createElement(CONFIG_XML_NAMESPACE);
            for (int i = 0; i < overlayCategory.size(); i++) {
                if (nodeListArray.get(i) != null) {
                    Node newNode = nodeListArray.get(i).cloneNode(true);
                    doc.adoptNode(newNode);
                    deviceElement.appendChild(newNode);
                }
            }
            // Make the new node an actual item in the target document
            rootElement.appendChild(deviceElement);
            doc.appendChild(rootElement);
        } catch (Exception e) {
            Log.e(TAG, e.getMessage());
        }

        return doc;
    }

    public static void saveXml(Document doc, String file) {
        try {
            TransformerFactory transformerFactory = TransformerFactory.newInstance();
            Transformer transformer = transformerFactory.newTransformer();
            DOMSource source = new DOMSource(doc);
            StreamResult result = new StreamResult(new File(file));
            // Output to console for testing
            // StreamResult result = new StreamResult(System.out);
            transformer.transform(source, result);
        } catch (Exception e) {
            Log.e(TAG, "saveXml fail ", e);
        }
    }

    public static Node getNode(Document doc, String expression) {
        Node node = null;
        XPath path = null;
        try {
            path = XPathFactory.newInstance().newXPath();
            node = (Node) path.compile(expression).evaluate(doc, XPathConstants.NODE);
        } catch (Exception e) {
            Log.e(TAG, e.getMessage());
        }
        return node;
    }

    public static Document GenerateOverrideXML(Document generic, Document overlay, String platform, String model, String sku) {
        ArrayList<Node> nodeListArray = new ArrayList<Node>();
        NodeList categoryNodes = getNode(generic, String.format(XML_ROOT_EXPRESSION, CONFIG_XML_TAG_GENERIC, CONFIG_XML_TAG_GENERIC, CONFIG_XML_TAG_GENERIC)).getChildNodes();

        for (int j = 0; j < categoryNodes.getLength(); j++) {
            if (categoryNodes.item(j).getNodeType() != Node.ELEMENT_NODE)
                continue;
            overlayCategory.add(((Element) categoryNodes.item(j)).getAttribute(DEFINE_NAME));
        }
        //for each overlay category
        for (int i = 0; i < overlayCategory.size(); i++) {
            Node node = proceedOverWrite(generic, overlay, overlayCategory.get(i), platform, model, sku);
            nodeListArray.add((Node) node);
        }

        Document newDoc = createDocWithNodes(nodeListArray, (Element)(getNode(generic, String.format(XML_ROOT_EXPRESSION, CONFIG_XML_TAG_GENERIC, CONFIG_XML_TAG_GENERIC, CONFIG_XML_TAG_GENERIC)).cloneNode(false)));
        newDoc.normalizeDocument();
        saveXml(newDoc, CONFIG_OVERRIDE_PATH);

        checkOverlayMD5(overlay, platform, model, sku);

        return newDoc;
    }

    private static Node proceedOverWrite(Document generic, Document overlay, String category, String platform, String model, String sku) {
        //Select all of the category nodes as overlayCategoryList
        //Select the root node of the category from generic xml.
        String parameter = String.format(XML_ROOT_EXPRESSION, CONFIG_XML_TAG_GENERIC, CONFIG_XML_TAG_GENERIC, CONFIG_XML_TAG_GENERIC) + "/" + String.format(XML_CATEGORY_EXPRESSION, category);
        Node genericRootNode = getNode(generic,  parameter);
        //Select all of the nodes of the category as genericNodeList
        NodeList genericnl = genericRootNode.getChildNodes();
        //Select all of the nodes of the category, platform and sku from overlay xml.
        NodeList nl;
        try {
            parameter = String.format(XML_ROOT_EXPRESSION, platform, model, sku) + "/" + String.format(XML_CATEGORY_EXPRESSION, category);
            nl = getNode(overlay, parameter).getChildNodes();
        } catch (Exception e) {
            Log.e(TAG, "No select device or sku data in the overlay.xml");
            return genericRootNode;
        }

        if (nl.getLength() > 0) {
            //clone all of the nodes which select from overlay xml.
            for (int i = 0; i < nl.getLength(); i++) {
                if (nl.item(i).getNodeType() != Node.ELEMENT_NODE)
                    continue;

                String itemName = ((Element) nl.item(i)).getAttribute(DEFINE_NAME);
                Node newNode = generic.importNode(nl.item(i), true);
                //Check generic xml has the same node which select from overlay xml or not.
                //If yes, than replace the node.If no, than add the node to the generic xml.
                if (findNode(generic, category, itemName) == true) {
                    genericRootNode = replaceNode(genericnl, genericRootNode, newNode, itemName);
                } else {
                    Log.e(TAG, "Overwrite xml fail: " + itemName + " can not be found in all.xml");
                    return null;
                }
            }
        }
        return genericRootNode;
    }

    private static Node replaceNode(NodeList genericnl, Node genericRootNode, Node newNode, String itemName) {
        for (int j = 0; j < genericnl.getLength(); j++) {
            if (genericnl.item(j).getNodeType() != Node.ELEMENT_NODE)
                continue;
            if (((Element) genericnl.item(j)).getAttribute(DEFINE_NAME).equals(itemName)) {
                if (((Element) newNode).getAttribute(DEFINE_ACTION).toLowerCase().equals(DEFINE_DELETE)) {
                    if (newNode.hasChildNodes() == true) {
                        Log.e(TAG, "Delete item format error.");
                    }
                    genericRootNode.removeChild(genericnl.item(j));
                } else {
                    genericRootNode.replaceChild(newNode, genericnl.item(j));
                }
            }
        }
        return genericRootNode;
    }

    public static boolean findNode(Document doc, String category, String itemName) {
        Node node = null;
        XPath path = null;

        try {
            path = XPathFactory.newInstance().newXPath();
            String expression = "/" + String.format(XML_ROOT_EXPRESSION, CONFIG_XML_TAG_GENERIC, CONFIG_XML_TAG_GENERIC, CONFIG_XML_TAG_GENERIC) + "/" + String.format(XML_CATEGORY_EXPRESSION, category) + "/" + String.format(XML_ITEM_EXPRESSION, itemName);
            node = (Node) path.compile(expression).evaluate(doc, XPathConstants.NODE);
        } catch (Exception e) {
            Log.e(TAG, e.getMessage());
            return false;
        }

        return true;
    }

    private static boolean checkOverlayMD5(Document overlay, String platform, String model, String sku) {
        String parameter = String.format(XML_ROOT_EXPRESSION, platform, model, sku);
        Node deviceNode = getNode(overlay, parameter);
        String overlayMD5 = ((Element)deviceNode).getAttribute("md5");
        Log.d(TAG, "overlay.xml MD5: " + overlayMD5);
        File file = new File(CONFIG_OVERRIDE_PATH);
        if(!file.exists()) {
            Log.e(TAG, CONFIG_OVERRIDE_PATH + "is not exist");
            return false;
        }

        Log.d(TAG, "override.xml MD5: " + MD5.calculateMD5(file));

        if(MD5.calculateMD5(file).equals(overlayMD5)) {
            Log.d(TAG, "MD5 check pass");
            return true;
        }
        else{
            Log.e(TAG, "MD5 check fail");
            return false;
        }
    }

    public static String GetXpathRoot(String category) {
        String xPath = String.format(XML_ROOT_EXPRESSION, CONFIG_XML_TAG_GENERIC, CONFIG_XML_TAG_GENERIC, CONFIG_XML_TAG_GENERIC) + "/" + String.format(XML_CATEGORY_EXPRESSION, category) + "/";
        return xPath;
    }

    public static String getNodeValue(String expression) {
        return ConfigFileParse.getNodeValue(expression);
    }

    public static NodeList getNodeList(Document doc, String expression) {
        NodeList nl = null;
        XPath path = null;
        try {
            path = XPathFactory.newInstance().newXPath();
            nl = (NodeList) path.compile(expression).evaluate(doc, XPathConstants.NODESET);
        } catch (Exception e) {
            Log.e(TAG, e.getMessage());
        }
        return nl;
    }
}
