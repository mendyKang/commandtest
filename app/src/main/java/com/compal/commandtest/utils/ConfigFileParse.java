package com.compal.commandtest.utils;

import java.io.File;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.w3c.dom.Document;

import android.os.Build;
import android.util.Log;

import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathFactory;

public class ConfigFileParse {
    private static final String TAG = ConfigFileParse.class.getSimpleName();

    public static ConfigFileParse configFileParse;

    private static String mFilePath = null;

    private static ConfigManager cm = null;
    private static ConfigManager co = null;

    public static final String regex =  "^[a-zA-Z]{1,}[0-9]{1,}(A[0-9]{1,}){0,1}";
    public static final Pattern pattern = Pattern.compile(regex);

    public ConfigFileParse() {
        try {
            String platform = Build.HARDWARE;
            String model = Build.MODEL;
            String sku = "";
            String version = "";
            if (platform.isEmpty()) {
                platform = "generic";
            }

            if (model.isEmpty()) {
                model = "generic";
            }

            if ((model.equals(platform) == true)&&(model.equals("generic") == false))
            {
                version = Build.VERSION.INCREMENTAL;
                final Matcher matcher = pattern.matcher(version);
                if(matcher.find())
                {
                    model = matcher.group(0);
                }
            }

            if (sku == null || sku.isEmpty()) {
                sku = "generic";
            }

            Log.d(TAG, "platform =" + platform + ", model =" + model +", sku =" + sku);

            cm = new ConfigManager(ConfigManager.CONFIG_DEFAULT_PATH, ConfigManager.CONFIG_FILE_ALL);
            co = new ConfigManager(ConfigManager.CONFIG_DEFAULT_PATH, ConfigManager.CONFIG_FILE_OVERLAY);
            cm.setDom(ConfigManager.GenerateOverrideXML(ConfigManager.getDom(ConfigManager.CONFIG_FILE_ALL),
                    ConfigManager.getDom(ConfigManager.CONFIG_FILE_OVERLAY), platform, model, sku));

        } catch (Exception e) {
            Log.e(TAG, "ConfigFileParse Error ", e);
        }
    }

    public static ConfigFileParse getInstance() {
        if (configFileParse == null)
            configFileParse = new ConfigFileParse();

        return configFileParse;
    }

    public static Document getDom(String category) {
        if (category == ConfigManager.CONFIG_FILE_ALL) {
            if (cm == null) {
                configFileParse = null;
                ConfigFileParse.getInstance();
            }
            return cm.xmlDocument;
        } else if (category == ConfigManager.CONFIG_FILE_OVERLAY) {
            if (co == null) {
                configFileParse = null;
                ConfigFileParse.getInstance();
            }
            return co.xmlDocument;
        } else {
            return null;
        }
    }

    /**
     * Set the config file path.
     *
     * @param filePath The file path of config files.
     */
    public static void setConfigFilePath(String filePath) {
        if (filePath.endsWith("/") == false) {
            filePath += "/";
        }
        mFilePath = filePath;
    }

    /**
     * Get the config file path.
     *
     * @return the config file path.
     */
    public static String getConfigFilePath() {
        return mFilePath;
    }

    private static boolean checkFileExists(String filePath) {
        File file = new File(filePath);
        if (!file.exists()) {
            return false;
        }
        return true;
    }

    public static String getNodeValue(String expression) {
        String nodeValue = "";
        XPath path = null;
        Document doc = null;

        doc = ConfigManager.getDom(ConfigManager.CONFIG_FILE_ALL);
        if (doc == null) {
            return null;
        }

        try {
            path = XPathFactory.newInstance().newXPath();
            nodeValue = path.compile(expression).evaluate(doc);
            Log.d(TAG, "expression =" + expression);
            Log.d(TAG, "nodeValue =" + nodeValue);
        } catch (Exception e) {
            Log.e(TAG, e.getMessage());
            return null;
        }

        return nodeValue;
    }
}
