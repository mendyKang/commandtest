package com.compal.commandtest.utils;
import static android.content.Context.MODE_PRIVATE;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Looper;
import android.util.Log;


import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class Util {
    public static final String TAG = Util.class.getSimpleName();
    public static int mJobId;
    public static Context mContext;
    public static SharedPreferences sharedPrefs;
    public final static String PREFS_NAME = "commandTest";
    public final static String KEY_PREF_TARGET_COUNT = "target_count";
    public final static String KEY_PREF_TEST_COUNT = "test_count";
    public final static String KEY_PREF_IS_REBOOT = "is_reboot";

    public static void init(Context context) {
        mContext = context;
        getSharedPreferences();
    }

    public static void copyConfigFile(){
        //Looper.prepare();
        copyFile(mContext, ConfigManager.CONFIG_FILE_ALL);
        copyFile(mContext, ConfigManager.CONFIG_FILE_OVERLAY);
    }

    public static SharedPreferences getSharedPreferences() {
        if (sharedPrefs == null)
            sharedPrefs = mContext.getSharedPreferences(PREFS_NAME,MODE_PRIVATE);
        return sharedPrefs;
    }

    public static void copyFile(Context context, String fileName){
        String tag = fileName;
        String outPath = "/data/data/" + context.getPackageName() + "/" + fileName;
        String sdcardPath = "sdcard/" + fileName;
        File file = new File(outPath);
        File sdcardFile = new File(sdcardPath);
        if (file.exists()){
            file.delete();
        }

        if (!file.exists()) {
            InputStream in = null;
            OutputStream out = null;
            try {
                Log.d(tag, "copy " + sdcardFile.getAbsolutePath() + " to " + outPath);
                if (sdcardFile.exists()){
                    Log.d(tag, "sdcard file exists");
                    in = new FileInputStream(sdcardFile);
                }else{
                    in = context.getAssets().open(fileName);
                }
                out = new FileOutputStream(file);
                int size = in.available();
                byte[] buf = new byte[size];
                int len;
                while ((len = in.read(buf)) > 0) {
                    out.write(buf, 0, len);
                }
            } catch (Exception e) {
                // TODO Auto-generated catch block
                Log.e(tag, "copy factlib error " + e.toString());

            } finally {
                if (in != null)
                    try {
                        in.close();
                    } catch (IOException e) {
                        // TODO Auto-generated catch block
                        Log.e(tag, "copy factlib close file error " + e.toString());
                    }
                if (out != null)
                    try {
                        out.close();
                    } catch (IOException e) {
                        // TODO Auto-generated catch block
                        Log.e(tag, "copy factlib close out file error " + e.toString());
                    }
            }
        }
    }

    public static String getVersionName(Context context){
        PackageManager packageManager=context.getPackageManager();
        PackageInfo packageInfo;
        String versionName="";
        try {
            packageInfo=packageManager.getPackageInfo(context.getPackageName(),0);
            versionName=packageInfo.versionName;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        return versionName;
    }
}
