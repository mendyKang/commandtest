package com.compal.commandtest;

import static android.text.Html.FROM_HTML_MODE_LEGACY;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckedTextView;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.compal.commandtest.model.EntityData;
import com.compal.commandtest.model.ExtrasData;
import com.compal.commandtest.model.TestResult;
import com.compal.commandtest.utils.ConfigFileParse;
import com.compal.commandtest.utils.ConfigManager;
import com.compal.commandtest.utils.Util;
import com.compal.easiplatform.ShellExecutor;

import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import io.reactivex.Completable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.functions.Action;
import io.reactivex.observers.DisposableCompletableObserver;
import io.reactivex.schedulers.Schedulers;

public class MainActivity extends AppCompatActivity {
    public final String TAG = MainActivity.class.getSimpleName();
    private TextView tvRecord, tvTestCount;
    private EditText etTargetCount;
    private Button btnStart, btnClear, btnExport;
    private CheckedTextView checkedTextView;

    private final int MSG_TEST_END = 1;
    private final int MSG_TEST_START = 2;
    private final int MSG_TEST_CLEAR = 3;
    private final int MSG_ENABLE_START_BUTTON = 4;
    private final int MSG_DISABLE_START_BUTTON = 5;
    private final int MSG_UPDATE_TITLE = 6;
    private final int MSG_UPDATE_HINT = 7;
    private final int MSG_EXPORT_FILE = 8;
    private final int MSG_PULL_FILE_DIALOG = 9;
    private final int MSG_CLEAR_DIALOG = 10;
    private final int DEFAULT_TARGET_COUNT = 10;
    private ShellExecutor shellexec = new ShellExecutor();

    private final String TEST_EXPRESSION = "item[@name='%s']/setting[@name='tests']/value[@stage='default']/entity";
    private final String TEST_FILE_EXPRESSION = "item[@name='%s']/setting[@name='tests_file_name']/value[@stage='default']";
    private Map<String, List<EntityData>> mapTest = new LinkedHashMap<String, List<EntityData>>();
    private Map<String, List<ExtrasData>> mapTestData = new LinkedHashMap<String, List<ExtrasData>>();
    private final String CONFIG_NOT_FOUND = "Config not defined";
    private final String CONFIG_DATA_LOST = "Config data lost";
    private final String ZERO_SEQUENCE = "00";
    private final String RESOURCE_TYPE_STRING = "string";
    private final String MESSAGE_KEY = "message_key";

    private final String XML_ATTR_NAME = "name";
    private final String XML_ATTR_RETRY = "retry";
    private final String XML_ATTR_INTERVAL = "interval";
    private final String XML_ATTR_MATCH_FIRST_GROUP = "match_first_group";
    private final String XML_ATTR_SEQUENCE = "sequence";
    private final String XML_ATTR_HINT = "hint";
    private final String XML_ATTR_REGEXP = "regexp";
    private final String XML_ATTR_COMMAND = "command";

    private List<String> listFailItems = new ArrayList<>();
    private Thread testThread = null;
    private NodeList testsNodeList;
    private String testsFileName;
    private int target_count, test_count;
    private boolean is_reboot;
    private boolean close_file = false;
    private ArrayList<String> testItemList;
    private Integer result_file_count = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setAppTitle();
        tvRecord = (TextView) findViewById(R.id.tvRecord);
        tvTestCount = (TextView) findViewById(R.id.tvCount);
        etTargetCount = (EditText) findViewById(R.id.etTargetCount);
        btnStart = (Button) findViewById(R.id.btnStart);
        btnClear = (Button) findViewById(R.id.btnClear);
        btnExport = (Button) findViewById(R.id.btnExport);
        checkedTextView = (CheckedTextView) findViewById(R.id.checkedTextView);

        getPermissions();
    }

    private void getValues() {
        target_count = Util.getSharedPreferences().getInt(Util.KEY_PREF_TARGET_COUNT, DEFAULT_TARGET_COUNT);
        test_count = Util.getSharedPreferences().getInt(Util.KEY_PREF_TEST_COUNT, 0);
        is_reboot = Util.getSharedPreferences().getBoolean(Util.KEY_PREF_IS_REBOOT, false);
        tvTestCount.setText(String.valueOf(test_count));
        etTargetCount.setText(String.valueOf(target_count));
        if (!is_reboot) {
            etTargetCount.setEnabled(false);
        }
        checkedTextView.setChecked(is_reboot);
    }

    private void getPermissions() {
        int writePermission = ActivityCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE);
        int readPermission = ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE);
        if (!(writePermission == PackageManager.PERMISSION_GRANTED &&
                readPermission == PackageManager.PERMISSION_GRANTED)) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE}, 100);
        } else {
            Log.d(TAG, "not requestPermissions");
            init();
        }
    }

    private void init() {
        getValues();
        Completable.fromAction(new Action() {
            @Override
            public void run() throws Exception {
                Util.copyConfigFile();
            }
        }).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new DisposableCompletableObserver() {
                    @Override
                    public void onComplete() {
                        Log.d(TAG, "init onComplete");
                        ConfigFileParse.getInstance();
                        loadConfig();
                        checkFileExist();
                        if (test_count != 0 && is_reboot) {
                            mHandler.sendEmptyMessage(MSG_TEST_START);
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.d(TAG, "CommandTestApplication init()", e);
                    }
                });
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case 100:
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Log.d(TAG, "PERMISSION_GRANTED");
                    init();
                } else {
                    Log.d(TAG, "language_noPermissions");
                }
                break;
        }
    }

    private class TestThread extends Thread {
        private String strEntity, strHint, matchFirstGroup;
        private int maxRetry;
        private int mMaxMatchCount = 0;
        private long testInterval;
        private int ITEM_TEST_RESULT = 1;
        private final int ITEM_TEST_PASS = 1;
        private final int ITEM_TEST_FAIL = 0;
        private int mMatchCount;
        private String testResults = "";

        @Override
        public void run() {
            List<TestResult> testResultList = new ArrayList<TestResult>();
            String text;
            mHandler.sendEmptyMessage(MSG_DISABLE_START_BUTTON);
            for (Map.Entry<String, List<EntityData>> entry : mapTest.entrySet()) {
                String keyEntity = entry.getKey().toString();

                Log.d(TAG, "keyEntity = " + keyEntity);
                if (!keyEntity.isEmpty() && !keyEntity.equals("")) {
                    text = "<font color=\"#0000FF\">" + String.format("Test Item : %s", keyEntity) + "</font><br>";
                    sendUpdateMessage(MSG_UPDATE_TITLE, text);

                    EntityData entityData = entry.getValue().get(0);
                    maxRetry = Integer.valueOf(entityData.getRetry());
                    testInterval = Long.valueOf(entityData.getInterval());
                    matchFirstGroup = entityData.getMatchFirstGroup();
                    Log.d(TAG, "maxRetry = " + maxRetry + ", " + "testInterval = " + testInterval +
                            ", " + "matchFirstGroup = " + matchFirstGroup);

                    List<ExtrasData> listExtra = mapTestData.get(keyEntity);
                    Log.d(TAG, "Count of extra data : listExtra.size() = " + listExtra.size());
                    mMaxMatchCount = listExtra.size() - 1;
                    for (ExtrasData data : listExtra) {
                        String mSeq = data.getSequence();
                        if (mSeq != null && !mSeq.equals("")) {
                            String command = data.getExtraCommand();
                            if (mSeq.equals(ZERO_SEQUENCE)) {
                                if (command != null && !command.equals("")) {
                                    //get the first test result will be combined with the later test results,
                                    //for comparison with the regular expression
                                    testResults = getLatestTestResult(testResults, command);
                                    Log.d(TAG, "testResults = " + testResults);
                                }
                                continue;
                            }
                            Pattern pattern = Pattern.compile(data.getRegExp());
                            Log.d(TAG, "regexp = " + data.getRegExp());
                            for (int i = 0; i < maxRetry; i++) {
                                testResults = getLatestTestResult(testResults, command);
                                text = "<font color=\"#4169E1\">" + String.format("result: %s , command: %s", testResults, command) + "</font><br>";
                                sendUpdateMessage(MSG_UPDATE_HINT, text);
                                Matcher matcher = pattern.matcher(testResults);
                                if (matcher.find()) {
                                    mMatchCount++;
                                    if (matchFirstGroup != null && matchFirstGroup.equals("true")) {
                                        testResults = matcher.group(0);
                                    }
                                    Log.d(TAG, "macther.find() - testResults = " + testResults);
                                    if (mMatchCount == mMaxMatchCount) {
                                        ITEM_TEST_RESULT &= ITEM_TEST_PASS;
                                        text = "<font color=\"#0000FF\">" + String.format("%s item: Pass", keyEntity) + "</font><br>";
                                        sendUpdateMessage(MSG_UPDATE_HINT, text);
                                        TestResult mData = new TestResult(keyEntity, "Pass");
                                        testResultList.add(mData);
                                    }
                                    break;
                                } else {
                                    if (i == maxRetry - 1) {
                                        //retry 300 times, test fail
                                        //add one flag to skip the rest tests for this item
                                        ITEM_TEST_RESULT &= ITEM_TEST_FAIL;
                                        text = "<font color=\"#FF0000\">" + String.format("%s item: Fail", keyEntity) + "</font><br>";
                                        sendUpdateMessage(MSG_UPDATE_HINT, text);
                                        TestResult mData = new TestResult(keyEntity, "Fail");
                                        testResultList.add(mData);
                                    } else {
                                        try {
                                            Thread.sleep(testInterval);
                                        } catch (InterruptedException e) {
                                            Log.e(TAG, e.getMessage());
                                        }
                                    }
                                }
                            }
                        } else {
                            text = "<font color=\"#FF0000\">" + String.format("%s \n", CONFIG_DATA_LOST) + "</font><br>";
                            sendUpdateMessage(MSG_UPDATE_HINT, text);
                        }
                        if ((ITEM_TEST_RESULT & ITEM_TEST_PASS) == 0) {
                            listFailItems.add(keyEntity);
                            for (String str : listFailItems) {
                                Log.d(TAG, "Fail items :" + str + ", ");
                            }
                            break;
                        }
                    }
                    mMatchCount = 0;
                    testResults = "";
                    ITEM_TEST_RESULT = 1;
                } else {
                    text = "<font color=\"#FF0000\">" + String.format("%s \n", CONFIG_DATA_LOST) + "</font><br>";
                    sendUpdateMessage(MSG_UPDATE_HINT, text);
                }
            }
            writeTestResultCSV(testResultList);
            testThread = null;
            mHandler.sendEmptyMessage(MSG_TEST_END);
        }
    }

    private void sendUpdateMessage(int handler_msg, String str) {
        Message msg = new Message();
        msg.what = handler_msg;
        Bundle bundle = new Bundle();
        bundle.putString(MESSAGE_KEY, str);
        msg.setData(bundle);
        mHandler.sendMessage(msg);
    }

    private String getLatestTestResult(String str, String command) {
        String output = getCommandResult(command);
        if (output.length() > 0) {
            str += output;
        }
        Log.d(TAG, "result:" + str + ", command:" + command);
        return str;
    }

    private String getCommandResult(String str) {
        return shellexec.execute(str);
    }

    public Handler mHandler = new Handler() {
        @SuppressLint("HandlerLeak")
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case MSG_UPDATE_TITLE:
                    tvRecord.setText(Html.fromHtml(msg.getData().get(MESSAGE_KEY).toString(), FROM_HTML_MODE_LEGACY));
                    break;
                case MSG_UPDATE_HINT:
                    tvRecord.append(Html.fromHtml(msg.getData().get(MESSAGE_KEY).toString(), FROM_HTML_MODE_LEGACY));
                    break;
                case MSG_TEST_START:
                    mHandler.sendEmptyMessage(MSG_DISABLE_START_BUTTON);
                    target_count = Integer.parseInt(etTargetCount.getText().toString());
                    Util.getSharedPreferences().edit()
                            .putInt(Util.KEY_PREF_TARGET_COUNT, target_count)
                            .apply();
                    test_count++;
                    Util.getSharedPreferences().edit()
                            .putInt(Util.KEY_PREF_TEST_COUNT, test_count)
                            .apply();
                    tvTestCount.setText(String.valueOf(test_count));
                    etTargetCount.setText(String.valueOf(target_count));
                    if (testThread == null) {
                        testThread = new TestThread();
                    }
                    testThread.start();
                    break;
                case MSG_TEST_END:
                    if (close_file) {
                        if (is_reboot) {
                            if (target_count > test_count) {
                                Handler handler = new Handler();
                                handler.postDelayed(new Runnable() {
                                    @Override
                                    public void run() {
                                        getCommandResult("reboot -p");
                                    }
                                }, 1000);
                            } else {
                                Util.getSharedPreferences().edit()
                                        .putBoolean(Util.KEY_PREF_IS_REBOOT, false)
                                        .apply();

                                etTargetCount.setEnabled(false);
                                is_reboot = Util.getSharedPreferences().getBoolean(Util.KEY_PREF_IS_REBOOT, false);
                                checkedTextView.setChecked(is_reboot);
                            }
                        }
                    } else {
                        Toast.makeText(MainActivity.this, "File not close completely!", Toast.LENGTH_LONG).show();
                    }
                    mHandler.sendEmptyMessage(MSG_ENABLE_START_BUTTON);
                    break;
                case MSG_TEST_CLEAR:
                    Util.getSharedPreferences().edit()
                            .putInt(Util.KEY_PREF_TEST_COUNT, 0)
                            .apply();
                    Util.getSharedPreferences().edit()
                            .putInt(Util.KEY_PREF_TARGET_COUNT, DEFAULT_TARGET_COUNT)
                            .apply();
                    Util.getSharedPreferences().edit()
                            .putBoolean(Util.KEY_PREF_IS_REBOOT, false)
                            .apply();
                    tvRecord.setText("");
                    result_file_count = 0;
                    close_file = false;
                    init();
                    break;
                case MSG_EXPORT_FILE:
                    mHandler.sendEmptyMessage(MSG_DISABLE_START_BUTTON);
                    ExportFailPassCSV();
                    if (close_file) {
                        mHandler.sendEmptyMessage(MSG_PULL_FILE_DIALOG);
                    }
                    break;
                case MSG_PULL_FILE_DIALOG:
                    mHandler.sendEmptyMessage(MSG_DISABLE_START_BUTTON);
                    AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                    builder.setTitle("Please pull the " + testsFileName + " file out");
                    builder.setMessage("adb pull " + testsFileName + "\nadb shell rm " + testsFileName);
                    builder.setPositiveButton("ok", null);
                    AlertDialog dialog = builder.create();
                    dialog.setCanceledOnTouchOutside(false);
                    dialog.show();
                    dialog.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            File file = new File(testsFileName);
                            if (file.exists()) {
                                Toast.makeText(MainActivity.this, "Please do the commands to pull the CSV file", Toast.LENGTH_SHORT).show();
                                return;
                            }
                            mHandler.sendEmptyMessage(MSG_TEST_CLEAR);
                            dialog.dismiss();
                            mHandler.sendEmptyMessage(MSG_ENABLE_START_BUTTON);
                        }
                    });
                    break;
                case MSG_CLEAR_DIALOG:
                    mHandler.sendEmptyMessage(MSG_DISABLE_START_BUTTON);
                    AlertDialog.Builder builder1 = new AlertDialog.Builder(MainActivity.this);
                    builder1.setTitle("Confirm to clear the test data");
                    builder1.setPositiveButton("Yes", null);
                    builder1.setNegativeButton("No", null);
                    AlertDialog dialog1 = builder1.create();
                    dialog1.setCanceledOnTouchOutside(false);
                    dialog1.show();
                    dialog1.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            File file = new File(testsFileName);
                            if (file.exists()) {
                                getCommandResult("rm " + testsFileName);
                            }
                            mHandler.sendEmptyMessage(MSG_TEST_CLEAR);
                            dialog1.dismiss();
                            mHandler.sendEmptyMessage(MSG_ENABLE_START_BUTTON);
                        }
                    });
                    dialog1.getButton(AlertDialog.BUTTON_NEGATIVE).setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            dialog1.dismiss();
                            mHandler.sendEmptyMessage(MSG_ENABLE_START_BUTTON);
                        }
                    });
                    break;
                case MSG_ENABLE_START_BUTTON:
                    btnStart.setEnabled(true);
                    btnExport.setEnabled(true);
                    btnClear.setEnabled(true);
                    checkedTextView.setEnabled(true);
                    break;
                case MSG_DISABLE_START_BUTTON:
                    btnStart.setEnabled(false);
                    btnExport.setEnabled(false);
                    btnClear.setEnabled(false);
                    if (!checkedTextView.isChecked()) {
                        checkedTextView.setEnabled(false);
                    }
                    break;
                default:
                    break;
            }
        }
    };

    private void loadConfig() {
        String entityName, retry, interval, matchFirstGroup;
        String sequence, hint, regexp, command;
        String xPathRoot = ConfigManager.GetXpathRoot(ConfigManager.CONFIG_XML_CATEGORY_ITEM);
        String curTestEx = xPathRoot + String.format(TEST_EXPRESSION, "CommandTest");
        String curTestFile = xPathRoot + String.format(TEST_FILE_EXPRESSION, "CommandTest");
        testItemList = new ArrayList<>();

        testsNodeList = ConfigManager.getNodeList(ConfigManager.getDom(ConfigManager.CONFIG_FILE_ALL), curTestEx);
        testsFileName = Environment.getExternalStorageDirectory() + "/" + ConfigManager.getNodeValue(curTestFile);

        if (testsNodeList.getLength() == 0) {
            Log.d(TAG, "config not found ");
            String text = "<font color=\"#FF0000\">" + String.format("%s \n", CONFIG_NOT_FOUND) + "</font><br>";
            sendUpdateMessage(MSG_UPDATE_HINT, text);
        } else {
            for (int i = 0; i < testsNodeList.getLength(); i++) {
                List<EntityData> listEntityData = new ArrayList<EntityData>();

                entityName = ((Element) testsNodeList.item(i)).getAttribute(XML_ATTR_NAME);
                retry = ((Element) testsNodeList.item(i)).getAttribute(XML_ATTR_RETRY);
                interval = ((Element) testsNodeList.item(i)).getAttribute(XML_ATTR_INTERVAL);
                matchFirstGroup = ((Element) testsNodeList.item(i)).getAttribute(XML_ATTR_MATCH_FIRST_GROUP);
                Log.d(TAG, "EntityName = " + entityName + ", " + "Retry = " + retry + ", " +
                        "Interval = " + interval + ", " + "matchFirstGroup = " + matchFirstGroup);
                EntityData entityData = new EntityData(retry, interval, matchFirstGroup);
                listEntityData.add(entityData);
                mapTest.put(entityName, listEntityData);
                testItemList.add(entityName);

                NodeList dataNodeList = testsNodeList.item(i).getChildNodes();
                List<ExtrasData> listExtraData = new ArrayList<ExtrasData>();

                for (int j = 0; j < dataNodeList.getLength(); j++) {
                    if (dataNodeList.item(j).getNodeType() == Node.ELEMENT_NODE) {
                        sequence = ((Element) dataNodeList.item(j)).getAttribute(XML_ATTR_SEQUENCE);
                        hint = ((Element) dataNodeList.item(j)).getAttribute(XML_ATTR_HINT);
                        regexp = ((Element) dataNodeList.item(j)).getAttribute(XML_ATTR_REGEXP);
                        command = ((Element) dataNodeList.item(j)).getAttribute(XML_ATTR_COMMAND);
                        Log.d(TAG, "Sequence = " + sequence + ", " + "Hint = " + hint + ", " +
                                "Regexp = " + regexp + ", " + "Command = " + command);
                        ExtrasData mData = new ExtrasData(sequence, hint, regexp, command);
                        listExtraData.add(mData);
                        mapTestData.put(entityName, listExtraData);
                    }
                }
            }
        }
    }

    public void startTest(View view) {
        mHandler.sendEmptyMessage(MSG_TEST_START);
    }

    public void resetData(View view) {
        mHandler.sendEmptyMessage(MSG_CLEAR_DIALOG);
    }

    public void autoCheckedTextView(View view) {
        checkedTextView.toggle();
        Util.getSharedPreferences().edit()
                .putBoolean(Util.KEY_PREF_IS_REBOOT, checkedTextView.isChecked())
                .apply();
        if (checkedTextView.isChecked()) {
            etTargetCount.setEnabled(true);
        } else {
            etTargetCount.setEnabled(false);
        }
        is_reboot = Util.getSharedPreferences().getBoolean(Util.KEY_PREF_IS_REBOOT, false);
    }

    public void exportFile(View view) {
        mHandler.sendEmptyMessage(MSG_EXPORT_FILE);
    }

    private void checkFileExist() {
        File file = new File(testsFileName);
        if (!file.exists()) {
            writeTestItemCSV();
        }
    }

    private void setAppTitle() {
        setTitle(String.format("%s         %s ", getTitle().toString(), Util.getVersionName(MainActivity.this)));
    }

    private void writeTestItemCSV() {
        File file = new File(testsFileName);
        try {
            FileOutputStream out = new FileOutputStream(file, true);
            StringBuilder sb = new StringBuilder();
            sb.append("test_count").append(",");
            for (int i = 0; i < testItemList.size(); i++) {
                sb.append(testItemList.get(i)).append(",");
            }
            sb.append("\n");
            out.write(sb.toString().getBytes());
            out.flush();
            out.close();
            close_file = true;
        } catch (Exception e) {
            close_file = false;
            e.printStackTrace();
        }
    }

    private void writeTestResultCSV(List<TestResult> testResultList) {
        this.runOnUiThread(new Runnable() {
            public void run() {
                Toast.makeText(MainActivity.this, "Start writing the CSV file.", Toast.LENGTH_SHORT).show();
            }
        });
        File file = new File(testsFileName);
        try {
            FileOutputStream out = new FileOutputStream(file, true);
            StringBuilder sb = new StringBuilder();
            sb.append(test_count).append(",");
            for (TestResult mTest : testResultList) {
                sb.append(mTest.getmResult()).append(",");
            }
            sb.append("\n");
            out.write(sb.toString().getBytes());
            out.flush();
            out.close();
            close_file = true;
        } catch (Exception e) {
            close_file = false;
            e.printStackTrace();
        }
    }

    private void ExportFailPassCSV() {
        this.runOnUiThread(new Runnable() {
            public void run() {
                Toast.makeText(MainActivity.this, "Start exporting the CSV file.", Toast.LENGTH_SHORT).show();
            }
        });
        File file = new File(testsFileName);
        Integer[] failList = null;
        StringBuilder sb = new StringBuilder();
        try {
            BufferedReader reader = new BufferedReader(new FileReader(file));
            String valueString = null;
            while ((valueString = reader.readLine()) != null) {
                String[] lineList = valueString.split(",");
                if (result_file_count == 0) {
                    failList = new Integer[lineList.length];
                    Arrays.fill(failList, 0);
                    sb.append("Fail / Pass").append(",");
                } else {
                    for (int i = 1; i < lineList.length; i++) {
                        if (lineList[i].equals("Fail")) {
                            failList[i] += 1;
                        }
                    }
                }
                result_file_count++;
            }
            reader.close();
            for (int i = 1; i < failList.length; i++) {
                sb.append(failList[i]).append("/").append(String.valueOf(test_count - failList[i])).append(",");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            FileOutputStream out = new FileOutputStream(file, true);
            sb.append("\n");
            out.write(sb.toString().getBytes());
            out.flush();
            out.close();
            close_file = true;
        } catch (Exception e) {
            close_file = false;
            e.printStackTrace();
        }
    }

}