package com.compal.commandtest;

import android.app.Application;
import android.util.Log;

import com.compal.commandtest.utils.ConfigFileParse;
import com.compal.commandtest.utils.Util;

import io.reactivex.Completable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.functions.Action;
import io.reactivex.observers.DisposableCompletableObserver;
import io.reactivex.schedulers.Schedulers;

public class CommandTestApplication extends Application {
    private static final String TAG = CommandTestApplication.class.getSimpleName();
    @Override
    public void onCreate() {
        super.onCreate();
        Log.d(TAG, "onCreateqq: "+CommandTestApplication.this.getApplicationInfo().dataDir);
        ConfigFileParse.setConfigFilePath(CommandTestApplication.this.getApplicationInfo().dataDir);
        Util.init(CommandTestApplication.this);
    }

    private void init() {

        Completable.fromAction(new Action() {
            @Override
            public void run() throws Exception {
                Util.init(CommandTestApplication.this);
            }
        }).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new DisposableCompletableObserver() {
                    @Override
                    public void onComplete() {
                        Log.d(TAG, "init onComplete");
                        ConfigFileParse.getInstance();
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.d(TAG, "CommandTestApplication init()", e);
                    }
                });
    }
}
