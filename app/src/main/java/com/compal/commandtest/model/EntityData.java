package com.compal.commandtest.model;

public class EntityData {
    private String mRetry;
    private String mInterval;
    private String mMatchFirstGroup;

    public EntityData(String retry, String interval, String matchFirstGroup) {
        this.mRetry = retry;
        this.mInterval = interval;
        this.mMatchFirstGroup = matchFirstGroup;
    }

    public String getRetry() {
        return this.mRetry;
    }

    public String getInterval() {
        return this.mInterval;
    }

    public String getMatchFirstGroup() {
        return this.mMatchFirstGroup;
    }
}
