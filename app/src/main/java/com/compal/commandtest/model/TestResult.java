package com.compal.commandtest.model;

public class TestResult {
    private String mTestItem;
    private String mResult;

    public TestResult(String testItem, String res) {
        mTestItem = testItem;
        mResult = res;
    }

    public String getmTestItem() {
        return mTestItem;
    }

    public void setmTestItem(String mTestItem) {
        this.mTestItem = mTestItem;
    }

    public String getmResult() {
        return mResult;
    }

    public void setmResult(String mResult) {
        this.mResult = mResult;
    }
}
