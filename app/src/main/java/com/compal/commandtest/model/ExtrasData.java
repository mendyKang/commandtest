package com.compal.commandtest.model;

public class ExtrasData {
    private String mSequence;
    private String mHint;
    private String mRegExp;
    private String mCommand;

    public ExtrasData(String sequence, String msg, String regexp, String command) {
        mSequence = sequence;
        mHint = msg;
        mRegExp = regexp;
        mCommand = command;
    }

    public String getSequence() {
        return this.mSequence;
    }

    public String getExtraHint() {
        return this.mHint;
    }

    public String getRegExp() {
        return this.mRegExp;
    }

    public String getExtraCommand() {
        return this.mCommand;
    }
}
